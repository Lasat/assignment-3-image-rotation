#include "file_manager.h"

bool open_file(FILE** file, char* filename, char* mode)
{
    *file = fopen(filename, mode);
    return *file != NULL;
}

void close_file(FILE** file) 
{
    if (*file == NULL)
        return;
    fclose(*file);
    *file = NULL;
}
