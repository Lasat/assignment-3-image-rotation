#include "bmp_format.h"

static bool read_header(FILE* file, struct bmp_header* header);
static bool write_header(FILE* file, struct bmp_header header);

void print_bmp_header (const struct bmp_header bmp_header)
{
    printf("bfType: %" PRIu16 "\n", bmp_header.bf_type);
    printf("bfileSize: %" PRIu32 "\n", bmp_header.bfile_size);
    printf("bfReserved: %" PRIu32 "\n", bmp_header.bf_reserved);
    printf("bOffBits: %" PRIu32 "\n", bmp_header.b_off_bits);
    printf("biSize: %" PRIu32 "\n", bmp_header.bi_size);
    printf("biWidth: %" PRIu32 "\n", bmp_header.bi_width);
    printf("biHeight: %" PRIu32 "\n", bmp_header.bi_height);
    printf("biPlanes: %" PRIu16 "\n", bmp_header.bi_planes);
    printf("biBitCount: %" PRIu16 "\n", bmp_header.bi_bit_count);
    printf("biCompression: %" PRIu32 "\n", bmp_header.bi_compression);
    printf("biSizeImage: %" PRIu32 "\n", bmp_header.bi_size_image);
    printf("biXPelsPerMeter: %" PRIu32 "\n", bmp_header.bi_x_pels_per_meter);
    printf("biYPelsPerMeter: %" PRIu32 "\n", bmp_header.bi_y_pels_per_meter);
    printf("biClrUsed: %" PRIu32 "\n", bmp_header.bi_clr_used);
    printf("biClrImportant: %" PRIu32 "\n", bmp_header.bi_clr_important);
}

enum read_status from_bmp(FILE* file, struct bmp_header* header, struct image* image)
{
    if (!read_header(file, header))
        return READ_HEADER_ERROR;

    if (header->bf_type != BMP_SIGNATURE)
        return NOT_BMP_ERROR;

    if (!allocate_image(header->bi_width, header->bi_height, image))
        return MEMORY_ERROR;


    uint8_t padding_bytes = (PADDING_BYTES - (header->bi_width * COUNT_OF_BYTES_IN_PIXEL) % PADDING_BYTES) % PADDING_BYTES;

    fseek(file, header->b_off_bits, SEEK_SET);

    for (size_t i=0; i< header->bi_height; i++) 
    {
        if(fread(image->data+i*header->bi_width, sizeof(struct pixel), header->bi_width, file) != header->bi_width) {
            free_image(image);
            return READ_PIXEL_ERROR;
        }
        
        fseek(file, padding_bytes, SEEK_CUR);
    }

    return READ_SUCCESS;
    
}

enum write_status to_bmp(FILE* file, const struct bmp_header header, const struct image image)
{
    if(!write_header(file, header))
        return WRITE_HEADER_ERROR;

    uint8_t padding_bytes = (PADDING_BYTES - (header.bi_width * COUNT_OF_BYTES_IN_PIXEL) % PADDING_BYTES) % PADDING_BYTES;

    fseek(file, header.b_off_bits, SEEK_SET);

    for (size_t i=0; i< header.bi_height; i++) 
    {
        if(fwrite(image.data+i*header.bi_width, sizeof(struct pixel), header.bi_width, file) != header.bi_width)
            return WRITE_PIXEL_ERROR;
        
        fseek(file, padding_bytes, SEEK_CUR);
    }

    return WRITE_SUCCESS;
}

static bool write_header(FILE* file, struct bmp_header header)
{
    return fwrite(&header, sizeof(struct bmp_header), 1, file) == 1;
}

static bool read_header(FILE* file, struct bmp_header* header)
{
    return fread(header, sizeof(struct bmp_header), 1, file) == 1;
}
