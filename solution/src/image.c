#include "image.h"
struct image rotate_image_counterclockwise(const struct image source)
{
    uint64_t width = source.height;
    uint64_t height = source.width;

    struct image new_image;
    if (!allocate_image(width, height, &new_image))
        return source;

    for(size_t i=0; i<height; i++)
    {
        for(size_t j=0; j<width; j++)
        {
            new_image.data[i*width+j] = source.data[(source.height-1-j)*source.width+i];
        }
    }
    
    return new_image;
}

bool allocate_image(uint64_t width, uint64_t height, struct image* image)
{
    struct pixel* data = malloc(sizeof(struct pixel)*width*height);

    if (data == NULL)
    {
        free(data);
        return false;
    }
    
    *image = (struct image){.width = width, .height = height, .data = data};

    return true;
}

void free_image(struct image* image)
{
    free(image->data);
}
