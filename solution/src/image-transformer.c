#include "bmp_format.h"
#include "file_manager.h"
#include "image.h"

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

static void update_header (struct bmp_header* header, struct image image);

int main(int argc, char** argv ) 
{   
    if(argc != 3)
    {
        printf("Not enough arguments");
        return EXIT_FAILURE;
    }
    FILE* file = NULL;
    if(!open_file(&file, argv[1], "rb"))
    {
        printf("Cannot open file ");
        printf("%s", argv[1]);
        printf("\n");
        return EXIT_FAILURE;
    }

    struct bmp_header header;
    struct image image;

    switch(from_bmp(file, &header, &image))
    {
        case READ_SUCCESS:
        {
            break;
        }
        case READ_PIXEL_ERROR:
        {
            printf("Pixel reading error\n");
            return EXIT_FAILURE;
        }
        case READ_HEADER_ERROR:
        {
            printf("Header reading error\n");
            return EXIT_FAILURE;
        }
        case MEMORY_ERROR:
        {
            printf("Cannot allocate memory\n");
            return EXIT_FAILURE;
        }
        case NOT_BMP_ERROR:
        {
            printf("%s", argv[1]);
            printf(" is not bmp file");
            return EXIT_FAILURE;
        }
    }

    close_file(&file);

    //print_bmp_header(header);

    struct image new_image = rotate_image_counterclockwise(image);

    update_header(&header, new_image);

    FILE* new_file;
    if(!open_file(&new_file, argv[2], "wb"))
    {
        printf("Cannot open ");
        printf("%s",argv[2]);
        printf("\n");
        return EXIT_FAILURE;
    }

    switch(to_bmp(new_file, header, new_image))
    {
        case WRITE_SUCCESS:
        {
            break;
        }
        case WRITE_PIXEL_ERROR:
        {
            printf("Write pixel error\n");
            return EXIT_FAILURE;
        }
        case WRITE_HEADER_ERROR:
        {
            printf("Write header error\n");
            return EXIT_FAILURE;
        }
    }

    free_image(&image);
    free_image(&new_image);

    close_file(&new_file);

    return EXIT_SUCCESS;
}

static void update_header (struct bmp_header* header, struct image image)
{
    header->bi_width = image.width;
    header->bi_height = image.height;
}
