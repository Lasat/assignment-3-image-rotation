#ifndef IMAGE_H
#define IMAGE_H

#include <inttypes.h>
#include <malloc.h>
#include <stdbool.h>
#include <stdint.h>

#define COUNT_OF_BYTES_IN_PIXEL 3
#define PADDING_BYTES 4

struct __attribute__((packed)) pixel { uint8_t b, g, r; };

struct image
{
    uint64_t width;
    uint64_t height;
    struct pixel* data;
};

struct image rotate_image_counterclockwise(struct image source);

bool allocate_image(uint64_t width, uint64_t height, struct image* image);

void free_image(struct image* image);

#endif
