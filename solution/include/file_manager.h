#ifndef FILE_MANAGER_H
#define FILE_MANAGER_H
#include <stdbool.h>
#include <stdio.h>

bool open_file(FILE** file, char* filename, char* mode);
void close_file(FILE** file);

#endif
