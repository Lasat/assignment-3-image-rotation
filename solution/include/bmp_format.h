#ifndef BMP_FORMAT_H
#define BMP_FORMAT_H

#include "image.h"
#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#define BMP_SIGNATURE 19778

struct __attribute__((packed)) bmp_header 
{
    uint16_t bf_type;
    uint32_t bfile_size;
    uint32_t bf_reserved;
    uint32_t b_off_bits;
    uint32_t bi_size;
    uint32_t bi_width;
    uint32_t bi_height;
    uint16_t bi_planes;
    uint16_t bi_bit_count;
    uint32_t bi_compression;
    uint32_t bi_size_image;
    uint32_t bi_x_pels_per_meter;
    uint32_t bi_y_pels_per_meter;
    uint32_t bi_clr_used;
    uint32_t bi_clr_important;
};

enum read_status {
    READ_SUCCESS,
    NOT_BMP_ERROR,
    READ_PIXEL_ERROR,
    READ_HEADER_ERROR,
    MEMORY_ERROR
};

enum write_status {
    WRITE_SUCCESS,
    WRITE_PIXEL_ERROR,
    WRITE_HEADER_ERROR
};

void print_bmp_header (const struct bmp_header bmp_header);

enum read_status from_bmp(FILE* file, struct bmp_header* header, struct image* image);
enum write_status to_bmp(FILE* file, const struct bmp_header header, const struct image image);

#endif
